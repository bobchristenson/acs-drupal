<?php
  

function acs_events_form($form, &$form_state) {
  
  $calendars = _acs_events_get_calendars();
  $calendar_array = array(0 => "No Calendars Found");
  if(empty($calendars)){
    drupal_set_message('Could not get Calendar list from ACS. Please report this to your web developer.', 'error');
  } else {
    $calendar_array = array();
    foreach($calendars as $calendar){
      $id = $calendar->CalendarId;
      $calendar_array[$id] = $calendar->Name;
    }  
  }

  $form['calendar'] = array(
    '#type' => 'select', 
    '#title' => t('Calendar'), 
    '#options' => $calendar_array, 
    '#default_value' => 0, 
    '#description' => t('Choose which calendar to act upon'),
  );
  
   $form['action'] = array(
    '#type' => 'select', 
    '#title' => t('Action'), 
    '#options' => array(
      'import' => t('Import All New Events'), 
      'update' => t('Update Existing Events'),
    ), 
    '#default_value' => 0, 
    '#description' => t('Choose which action to perform'),
  );

   $form['length'] = array(
    '#type' => 'select', 
    '#title' => t('Time Frame'), 
    '#options' => array(
      '0' => t('Entire Current Month'),
      '1' => t('Today + 1 Month'), 
      '2' => t('Today + 2 Months'),
      '6' => t('Today + 6 Months'),
      '9' => t('Today + 9 Months'),
      '12' => t('Today + 1 Year'),
    ), 
    '#default_value' => 0, 
    '#description' => t('How far out do you want events from?<br /><strong>Note the longer the time frame, the more room for errors if there are alot of events!</strong>.<br /><em>Past events cannot be imported except for when using "Entire Current Month".</em>'),
  );
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Update Events'),
  );
  
  return $form;
}


function acs_settings_form($form, &$form_state) { 
  
  //ACS SETTINGS
  $form['acs'] = array(
    '#type' => 'fieldset',
    '#title' => t('ACS Credentials'),
    '#collapsible' => TRUE, // Added
    '#collapsed' => FALSE,  // Added
  );
  
  $form['acs']['acs_event_siteid'] = array(
    '#type' => 'textfield', 
    '#size' => 10,
    '#title' => t('Site ID'), 
    '#required' => TRUE,
    '#default_value' => variable_get('acs_event_siteid'),
    '#description' => t('Enter your ACS Site ID'),
  );
  
   $form['acs']['acs_event_username'] = array(
    '#type' => 'textfield', 
    '#size' => 20,
    '#required' => TRUE,
    '#title' => t('API Username'), 
    '#default_value' => variable_get('acs_event_username'),
    '#description' => t('Enter your ACS API Access Username'),
  );

   $form['acs']['acs_event_password'] = array(
    '#type' => 'textfield', 
    '#size' => 20,
    '#required' => TRUE,
    '#title' => t('API Password'), 
    '#default_value' => variable_get('acs_event_password'),
    '#description' => t('Enter your ACS API Access Password'),
  );
  
  //NODE SETTINGS
  $form['eventnode'] = array(
    '#type' => 'fieldset',
    '#title' => t('Event Node Settings'),
    '#collapsible' => TRUE, // Added
    '#collapsed' => FALSE,  // Added
  );
  
  
  //build node type array
  $type_array = array('0' => 'No Node Types Available');
  $types = node_type_get_types();
  if($types){
    $type_array = array();
    foreach($types as $type){
      $type_array[$type->type] = $type->name;
    }
  }
  
  $form['eventnode']['acs_event_nodetype'] = array(
    '#type' => 'select', 
    '#title' => t('Event Node Type'), 
    '#options' => $type_array,
    '#required' => TRUE,
    '#default_value' => variable_get('acs_event_nodetype'), 
    '#description' => t('Choose your Event nodetype'),
  );
  
  //build a list of fields
  $fields = field_info_field_map();
  $textfield_array = array();
  $datefield_array = array();
  if($fields){
    foreach($fields as $key => $value) {
      if($value['type'] == 'text'){
        $textfield_array[$key] = $key;
      }
      if($value['type'] == 'date'){
        $datefield_array[$key] = $key;
      }
    }
  }
  
  $form['eventnode']['acs_event_datefield'] = array(
    '#type' => 'select', 
    '#title' => t('Field to Store Event Date'), 
    '#options' => $datefield_array,
    '#required' => TRUE,
    '#default_value' => variable_get('acs_event_datefield'), 
    '#description' => t('The Event Date Field on your Event Node'),
  );
  
  $form['eventnode']['acs_event_idfield'] = array(
    '#type' => 'select', 
    '#title' => t('Field to Store Master Event ID'), 
    '#options' => $textfield_array,
    '#required' => TRUE,
    '#default_value' => variable_get('acs_event_idfield'), 
    '#description' => t('A textfield is required to store a unique event identifier for the <strong>master</strong> event. When an event repeats, this is the field that keeps them grouped together.'),
  );

  $form['eventnode']['acs_event_instancefield'] = array(
    '#type' => 'select', 
    '#title' => t('Field to Store Event Instance ID'), 
    '#options' => $textfield_array,
    '#required' => TRUE,
    '#default_value' => variable_get('acs_event_instancefield'), 
    '#description' => t('<strong>Be sure this is different than your Event ID field!</strong>  A textfield is required to store a unique event instance identifier.  This identifies unique instances of events and creates each instance as a Drupal node.'),
  );
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save'),
  );
  
  return $form;
}


/*do the query, call the excecute function */
function acs_events_form_submit($form, &$form_state) {
  $cal = $form_state['values']['calendar'];
  $action = $form_state['values']['action'];
  $length = $form_state['values']['length'];
  $acs_events = _acs_events_get_events($cal, $length);
  _acs_events_process_events($acs_events, $action);
  
}

function acs_settings_form_submit($form, &$form_state) {
    variable_set('acs_event_siteid', $form_state['values']['acs_event_siteid']);
    variable_set('acs_event_username', $form_state['values']['acs_event_username']);
    variable_set('acs_event_password', $form_state['values']['acs_event_password']);
    variable_set('acs_event_nodetype', $form_state['values']['acs_event_nodetype']);
    variable_set('acs_event_idfield', $form_state['values']['acs_event_idfield']);
    variable_set('acs_event_instancefield', $form_state['values']['acs_event_instancefield']);
    variable_set('acs_event_datefield', $form_state['values']['acs_event_datefield']);
    
    drupal_set_message('Settings Saved!');
}

