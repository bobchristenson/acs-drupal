Import Events from ACS: http://www.acstechnologies.com/
API Docs: http://wiki.acstechnologies.com/display/DevCom/Working+with+the+API
Requires Access ACS Membership

Enter Site ID, Username, Password in the settings screen: /admin/content/acs-events/settings
Import Events at: /admin/content/acs-events

Right now this takes a specific configuration of an event node, so you'll have to look at the code
to see what we expect and how it works.  Pretty custom module at this point.